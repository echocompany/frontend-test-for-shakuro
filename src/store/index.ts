import { store } from 'quasar/wrappers';
import Vuex from 'vuex';

import operatorsModule from './module-operators';
import { OperatorsStateInterface } from './module-operators/state';

export interface StateInterface {
  operatorsModule: OperatorsStateInterface;
}

export default store(({ Vue }) => {
  Vue.use(Vuex);

  const Store = new Vuex.Store<StateInterface>({
    modules: {
      operatorsModule,
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: !!process.env.DEBUGGING,
  });

  return Store;
});
