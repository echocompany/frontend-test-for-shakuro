import { Operator } from 'components/models';

export interface OperatorsStateInterface {
  operators: Operator[]
}

function state(): OperatorsStateInterface {
  return {
    operators: [
      {
        name: 'МТС',
        slug: 'mts',
        img: 'https://tumen.justconnect.ru/plugins/provider/uploads/logo/323/provider_logo_323_839577096_500.png?rnd=1572291064',
      },
      {
        name: 'Билайн',
        slug: 'beeline',
        img: 'https://www.magellan-mfc.ru/sites/default/files/inx960x640_0.jpg',
      },
      {
        name: 'Мегафон',
        slug: 'megafon',
        img: 'https://pbs.twimg.com/profile_images/1273513441479790592/zKtu5G81_400x400.jpg',
      },
    ],
  };
}

export default state;
