import { Module } from 'vuex';
import { StateInterface } from '../index';
import state, { OperatorsStateInterface } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const operatorsModule: Module<OperatorsStateInterface, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default operatorsModule;
