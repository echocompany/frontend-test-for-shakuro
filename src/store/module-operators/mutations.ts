import { MutationTree } from 'vuex';
import { OperatorsStateInterface } from './state';

const mutation: MutationTree<OperatorsStateInterface> = {
  someMutation(/* state: ExampleStateInterface */) {
    // your code
  },
};

export default mutation;
