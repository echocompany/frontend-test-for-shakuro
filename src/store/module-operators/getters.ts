import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { OperatorsStateInterface } from './state';

const getters: GetterTree<OperatorsStateInterface, StateInterface> = {
  getOperators(state) {
    return state.operators;
  },
};

export default getters;
