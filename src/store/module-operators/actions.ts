import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { OperatorsStateInterface } from './state';

const actions: ActionTree<OperatorsStateInterface, StateInterface> = {
  replenishmentBalance(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      if (Math.random() < 0.5) {
        resolve(true);
      }
      reject(false);
    });
  },
};

export default actions;
