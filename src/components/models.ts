export interface Operator {
  name: string;
  slug: string;
  img: string;
}
