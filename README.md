# frontend test for shakuro

## Refactoring
```javascript
function func(s, a, b) {
  let i = s.length - 1;

  while (i >= 0) {
    if (s[i] === a) {
      return i;
    }
    if (s[i] === b) {
      return i;
    }
    i -= 1;
  }
  return -1;
}
```

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
